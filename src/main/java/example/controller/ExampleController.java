package example.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class ExampleController {
    @Autowired
    private NameBean nameBean;

    @GetMapping("/")
    public String index() {
        return "index.html";
    }

    @PostMapping("/save")
    public void save(String name, HttpServletResponse response) throws IOException {
        nameBean.setName(name);
        response.sendRedirect("/view");
    }

    @GetMapping("/view")
    public String view(Model model) {
        model.addAttribute("nameBean", nameBean);
        return "view.html";
    }
}